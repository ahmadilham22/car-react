import React from "react";
import { useEffect } from "react";
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import Faq from "../components/Accordion/Faq";
import Banner from "../components/Banner/Banner";
import Main from "../components/Main/Main";
import Testimonial from "../components/Testimonial/Testimonial";
import WhyUs from "../components/WhyUs/WhyUs";
import OurServices from "../components/OurServices/OurServices";

const LandingPage = () => {
  useEffect(() => {
    document.title = "Homepage";
  }, []);

  return (
    <div>
      <Header />
      <Main />
      <OurServices />
      <WhyUs />
      <Testimonial />
      <Banner />
      <Faq />
      <Footer />
    </div>
  );
};

export default LandingPage;
