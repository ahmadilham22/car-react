import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import AOS from 'aos';

const Main = () => {
  return (
    <div>
      {' '}
      <div className="container-fluid">
        <div className="row main-content-1 d-flex">
          <div className="col-6 main-contents">
            <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
            <p>
              Selamat datang di Binar Car Rental. Kami menyediakan mobil
              kualitas
              <br />
              terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
              <br />
              untuk sewa mobil selama 24 jam.
            </p>
          </div>
          <div className="col-6 fix-img">
            <img src="./IMG/img_car.png" className="img-fluid" alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
