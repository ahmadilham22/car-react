import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import AOS from 'aos';
import { useEffect, useRef } from 'react';
import Typed from 'typed.js';

const Main = () => {
  const el = useRef(null);
  useEffect(() => {
    AOS.init();
    const typed = new Typed(el.current, {
      strings: [`Sewa Mobil Kawasan Kabupaten Kerinci`], // Strings to display
      // Speed settings, try diffrent values untill you get good results
      startDelay: 100,
      typeSpeed: 50,
      backSpeed: 50,
      backDelay: 0,
    });
    return () => {
      typed.destroy();
    };
  }, []);
  return (
    <div>
      <div className="container-fluid">
        <div className="row main-content-1">
          <div className="col-6 main-contents" data-aos="flip-left">
            <h1 ref={el}></h1>
            <p className="mt-3">
              Selamat datang di Binar Car Rental. Kami menyediakan mobil
              kualitas
              <br />
              terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
              <br />
              untuk sewa mobil selama 24 jam.
            </p>
            <Link to="/cars">
              <button type="button" className="btn">
                Mulai Sewa Mobil
              </button>
            </Link>
          </div>
          <div className="col-6 fix-img" data-aos="flip-right">
            <img src="./IMG/img_car.png" className="img-fluid" alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
