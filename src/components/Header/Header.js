import React from 'react';
import './Header.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const Header = () => {
  return (
    <div>
      <nav className="navbar navbar-expand main-nav">
        <div className="container">
          <a className="navbar-brand" href="#">
            <img src="./IMG/logo.png" alt="" />
          </a>

          <div className="collapse navbar-collapse menu-nav" id="navbarNav">
            <ul className="navbar-nav ms-auto">
              <li className="nav-item">
                <a className="nav-link" href="#OurServices">
                  Our Services
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#WhyUs">
                  Why Us
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#Testimonial">
                  Testimonial
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#FAQ">
                  FAQ
                </a>
              </li>
              <li className="nav-item">
                <a className="btn nav-button">Register</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Header;
