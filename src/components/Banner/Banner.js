import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import AOS from 'aos';
import { useEffect } from 'react';

const Banner = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <div>
      <div className="container mt-3 main-banner">
        <div className="mt-4 p-5 text-white rounded text-center box">
          <h1 className="pb-4" data-aos="fade-up" data-aos-delay="100">
            Sewa Mobil di (Lokasimu) Sekarang
          </h1>
          <p data-aos="fade-up" data-aos-delay="100">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod <br />
            tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <button className="btn mt-5" data-aos="fade-up" data-aos-delay="100">
            Mulai Sewa Mobil
          </button>
        </div>
      </div>
    </div>
  );
};

export default Banner;
