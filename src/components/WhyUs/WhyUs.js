import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import AOS from 'aos';
import { useEffect } from 'react';

const WhyUs = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <div>
      <section>
        <div className="container slip-text pt-5 pb-5">
          <div className="row">
            <div className="col" id="WhyUs">
              <h1>Why Us?</h1>
              <p>Mengapa harus pilih Binar Car Rental?</p>
            </div>
          </div>
          <div className="row card-content">
            <div className="col-3" data-aos="fade-right" data-aos-delay="100">
              <div className="card">
                <img
                  src="./IMG/icon_complete.png"
                  className="m-3 img-fluid"
                  width="40px"
                  height="40px"
                  alt=""
                />
                <div className="card-body">
                  <h4>Mobil Lengkap</h4>
                  <p className="card-text">
                    Tersedia banyak pilihan mobil, <br />
                    kondisi masih baru, bersih dan terawat
                  </p>
                </div>
              </div>
            </div>
            <div className="col-3" data-aos="fade-right" data-aos-delay="200">
              <div className="card card-content">
                <img
                  src="./IMG/icon_price.png"
                  className="m-3 img-fluid"
                  width="40px"
                  height="40px"
                  alt=""
                />
                <div className="card-body">
                  <h4>Harga Murah</h4>
                  <p className="card-text">
                    Harga murah dan bersaing, bisa bandingkan harga kami dengan
                    rental mobil lain
                  </p>
                </div>
              </div>
            </div>
            <div className="col-3" data-aos="fade-right" data-aos-delay="300">
              <div className="card card-content">
                <img
                  src="./IMG/icon_24hrs.png"
                  className="m-3 img-fluid"
                  width="40px"
                  height="40px"
                  alt=""
                />
                <div className="card-body">
                  <h4>Layanan 24 Jam</h4>
                  <p className="card-text">
                    Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami
                    juga tersedia di akhir minggu
                  </p>
                </div>
              </div>
            </div>
            <div className="col-3" data-aos="fade-right" data-aos-delay="400">
              <div className="card card-content">
                <img
                  src="./IMG/icon_professional.png"
                  className="m-3 img-fluid"
                  width="40px"
                  height="40px"
                  alt=""
                />
                <div className="card-body">
                  <h4>Sopir Profesional</h4>
                  <p className="card-text">
                    Sopir yang profesional, berpengalaman, jujur, ramah dan
                    selalu tepat waktu
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default WhyUs;
