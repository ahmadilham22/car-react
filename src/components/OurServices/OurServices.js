import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import AOS from 'aos';
import { useEffect } from 'react';

const OurServices = () => {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <div>
      {' '}
      <div className="container main-content-2" id="OurServices">
        <div className="row ms-5 ps-5">
          <div
            className="col-lg-5 ms-5 ps-5"
            data-aos="fade-up"
            data-aos-delay="150"
          >
            <img
              src="./IMG/img_service.png"
              className="img-fluid"
              width="374px"
              height="366px"
              alt=""
            />
          </div>
          <div className="col-lg-6 ms-5 ps-5 main-contents-2">
            <h1 data-aos="fade-up">
              Best Car Rental for any kind of trip in <br />
              (Lokasimu)!
            </h1>
            <br />
            <p data-aos="fade-up">
              Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
              lebih <br />
              murah dibandingkan yang lain, kondisi mobil baru, serta kualitas
              <br />
              pelayanan terbaik untuk perjalanan wisata, bisnis, wedding,
              meeting, dll.
            </p>
            <ul className="ps-0">
              <li data-aos="fade-up" data-aos-delay="200">
                <img
                  src="./IMG/Group 53.png"
                  className="img-fluid me-3 "
                  width="20px"
                  height="20px"
                  alt=""
                />
                Sewa Mobil Dengan Supir di Bali 12 Jam
              </li>
              <br />
              <li data-aos="fade-up" data-aos-delay="300">
                <img
                  src="./IMG/Group 53.png"
                  className="img-fluid me-3"
                  width="20px"
                  height="20px"
                  alt=""
                />
                Sewa Mobil Lepas Kunci di Bali 24 Jam
              </li>
              <br />
              <li data-aos="fade-up" data-aos-delay="400">
                <img
                  src="./IMG/Group 53.png"
                  className="img-fluid me-3"
                  width="20px"
                  height="20px"
                  alt=""
                />
                Sewa Mobil Jangka Panjang Bulanan
              </li>
              <br />
              <li data-aos="fade-up" data-aos-delay="500">
                <img
                  src="./IMG/Group 53.png"
                  className="img-fluid me-3"
                  width="20px"
                  height="20px"
                  alt=""
                />
                Gratis Antar - Jemput Mobil di Bandara
              </li>
              <br />
              <li data-aos="fade-up" data-aos-delay="600">
                <img
                  src="./IMG/Group 53.png"
                  className="img-fluid me-3"
                  width="20px"
                  height="20px"
                  alt=""
                />
                Layanan Airport Transfer / Drop In Out
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurServices;
