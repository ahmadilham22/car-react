import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';
import {
  // Container,
  Row,
  Col,
  Card,
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Testimonial.css';
import AOS from 'aos';
import { useEffect } from 'react';

function Testimonial() {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <div id="testimonial" className="mb-5">
      <div className="slip-text mt-5">
        <h1>Testimonial</h1>
        <p>Berbagai review positif dari pelanggan kami</p>
      </div>
      <Row>
        <OwlCarousel
          className="owl-theme"
          data-aos="fade-right"
          loop={true}
          margin={32}
          autoHeight={true}
          nav={true}
          center={true}
          dots={false}
          navText={[
            "<img src='./IMG/leftbutton.png' style='width: 32px; margin-right: 10px;'>",
            "<img src='./IMG/rightbutton.png' style='width: 32px;'>",
          ]}
          responsive={{
            0: {
              items: 1,
            },
            800: {
              items: 1,
            },
            1000: {
              items: 2,
            },
          }}
        >
          <Row>
            <Card className="card-testi">
              <Card.Body>
                <Row>
                  <Col className="testi-photo">
                    <img
                      src={process.env.PUBLIC_URL + './IMG/img_photo1.png'}
                      alt=""
                    />
                  </Col>
                  <Col className="testi" sm={8}>
                    <img
                      src={process.env.PUBLIC_URL + './IMG/Rate.png'}
                      className="rate"
                      alt=""
                    />
                    <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Fuga asperiores qui omnis, ex possimus eligendi dolorum
                      cumque reiciendis provident, ipsam ea quis incidunt rerum
                      laboriosam! Corrupti, eum. Et, dolore minus.
                    </p>
                    <p className="username">John Dee, 32 Bromo</p>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Row>
          <Row>
            <Card className="card-testi">
              <Card.Body>
                <Row>
                  <Col className="testi-photo">
                    <img
                      src={process.env.PUBLIC_URL + './IMG/img_photo2.png'}
                      alt=""
                    />
                  </Col>
                  <Col className="testi" sm={8}>
                    <img
                      src={process.env.PUBLIC_URL + './IMG/Rate.png'}
                      className="rate"
                      alt=""
                    />
                    <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Fuga asperiores qui omnis, ex possimus eligendi dolorum
                      cumque reiciendis provident, ipsam ea quis incidunt rerum
                      laboriosam! Corrupti, eum. Et, dolore minus.
                    </p>
                    <p className="username">John Dee, 32 Bromo</p>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Row>
          <Row>
            <Card className="card-testi">
              <Card.Body>
                <Row>
                  <Col className="testi-photo">
                    <img
                      src={process.env.PUBLIC_URL + './IMG/img_photo1.png'}
                      alt=""
                    />
                  </Col>
                  <Col className="testi" sm={8}>
                    <img
                      src={process.env.PUBLIC_URL + './IMG/Rate.png'}
                      className="rate"
                      alt=""
                    />
                    <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Fuga asperiores qui omnis, ex possimus eligendi dolorum
                      cumque reiciendis provident, ipsam ea quis incidunt rerum
                      laboriosam! Corrupti, eum. Et, dolore minus.
                    </p>
                    <p className="username">John Dee, 32 Bromo</p>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Row>
        </OwlCarousel>
      </Row>
    </div>
  );
}

export default Testimonial;
