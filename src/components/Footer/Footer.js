import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

const Footer = () => {
  return (
    <div className="section-footer mt-5">
      <div className="container footer">
        <div className="row">
          <div className="col-3">
            <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
            <p>binarcarrental@gmail.com</p>
            <p>081-233-334-808</p>
          </div>
          <div className="col-3">
            <p>Our services</p>
            <p>Why Us</p>
            <p>Testimonial</p>
            <p>FAQ</p>
          </div>
          <div className="col-3">
            <p>Connect with us</p>
            <ul className="ps-0">
              <li>
                <img
                  src="./IMG/icon_facebook.png"
                  width="32px"
                  height="32px"
                  alt=""
                />
              </li>
              <li>
                <img
                  src="./IMG/icon_instagram.png"
                  width="32px"
                  height="32px"
                  alt=""
                />
              </li>
              <li>
                <img
                  src="./IMG/icon_twitter.png"
                  width="32px"
                  height="32px"
                  alt=""
                />
              </li>
              <li>
                <img
                  src="./IMG/icon_mail.png"
                  width="32px"
                  height="32px"
                  alt=""
                />
              </li>
              <li>
                <img
                  src="./IMG/icon_twitch.png"
                  width="32px"
                  height="32px"
                  alt=""
                />
              </li>
            </ul>
          </div>
          <div className="col-3">
            <p>Copyright Binar 2022</p>
            <img
              src="./IMG/Rectangle 74.png"
              width="100px"
              height="34px"
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
